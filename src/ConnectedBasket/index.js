import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  emptyBasket,
  removeProduct,
  updateProductQuantity,
} from '../redux/actions';
import Basket from '../Basket';

const mapStateToProps = state => ({
  items: state.products.filter(({ quantity }) => quantity > 0),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      emptyBasket,
      removeProduct,
      updateProductQuantity,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Basket);
