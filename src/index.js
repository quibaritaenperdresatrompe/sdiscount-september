import { createStore } from 'redux';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';

import './styles.css';
import App from './ConnectedApp';
import registerServiceWorker from './registerServiceWorker';
import reducers from './redux/reducers';

const store = createStore(
  reducers,
  process.env.NODE_ENV === 'DEV' && window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : undefined,
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
registerServiceWorker();
