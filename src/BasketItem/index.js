import { func, number, string } from 'prop-types';
import React, { Component } from 'react';

import Button from '../Button';
import Selector from '../Selector';
import './styles.css';

class Basket extends Component {
  constructor(props) {
    super(props);

    const { quantity } = this.props;

    this.state = {
      selectedQuantity: quantity,
    };
  }

  handleRemoveButtonClick = () => {
    const { reference, removeProduct } = this.props;

    removeProduct(reference);
  };

  handleQuantitySelectorChange = event => {
    if (event && event.target && event.target.value) {
      const selectedQuantity = parseInt(event.target.value, 10);

      this.setState(
        () => ({
          selectedQuantity,
        }),
        this.updateProductQuantity,
      );
    }
  };

  updateProductQuantity() {
    const { quantity, reference, updateProductQuantity } = this.props;
    const { selectedQuantity } = this.state;
    const deltaQuantity = selectedQuantity - quantity;

    updateProductQuantity(reference, deltaQuantity);
  }

  renderButton = () => (
    <div className="basket-item-remove-button">
      <Button
        action={this.handleRemoveButtonClick}
        size="small"
        title="Supprimer"
      />
    </div>
  );

  renderQuantitySelector() {
    const { quantity } = this.props;
    const availableQuantities = [1, 2, 3, 4, 5, 10];
    const extendedAvailableQuantities = availableQuantities.includes(quantity)
      ? availableQuantities
      : [...availableQuantities, quantity];
    const sortedAvailableQuantites = extendedAvailableQuantities.sort(
      (a, b) => a - b,
    );
    const options = sortedAvailableQuantites.map(quantity => ({
      label: quantity.toString(),
      value: quantity.toString(),
    }));

    return (
      <Selector
        value={quantity.toString()}
        onChange={this.handleQuantitySelectorChange}
        options={options}
      />
    );
  }

  render() {
    const { price, quantity, reference, title } = this.props;

    return (
      <li className="basket-item" key={reference}>
        <div className="basket-item-title">{`${reference} - ${title}`}</div>
        <div className="basket-item-quantity">
          {this.renderQuantitySelector()}
        </div>
        <div className="basket-item-unit">{`${price.toFixed(2)} €`}</div>
        <div className="basket-item-total">{`${(quantity * price).toFixed(
          2,
        )} €`}</div>
        {this.renderButton()}
      </li>
    );
  }
}

Basket.defaultProps = {
  price: 0,
  quantity: 0,
  reference: '',
  removeProduct: Function.prototype,
  title: '',
  updateProductQuantity: Function.prototype,
};

export const basketItemTypes = {
  price: number,
  quantity: number,
  reference: string,
  title: string,
};

Basket.propTypes = {
  ...basketItemTypes,
  removeProduct: func,
  updateProductQuantity: func,
};

export default Basket;
