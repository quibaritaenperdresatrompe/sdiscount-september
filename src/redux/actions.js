import {
  EMPTY_BASKET,
  REMOVE_PRODUCT,
  SET_PRODUCTS,
  UPDATE_PRODUCT_QUANTITY,
} from './actionTypes';

export const emptyBasket = () => ({
  type: EMPTY_BASKET,
});

export const removeProduct = productReference => ({
  type: REMOVE_PRODUCT,
  payload: {
    productReference,
  },
});

export const setProducts = products => ({
  type: SET_PRODUCTS,
  payload: {
    products,
  },
});

export const updateProductQuantity = (productReference, deltaQuantity) => ({
  type: UPDATE_PRODUCT_QUANTITY,
  payload: {
    deltaQuantity,
    productReference,
  },
});
