import { func, oneOf, string } from 'prop-types';
import React, { Component } from 'react';

import { StyledButton } from './styles';

class Button extends Component {
  render() {
    const { action, backgroundColor, color, size, title } = this.props;

    return (
      <StyledButton
        onClick={action}
        size={size}
        backgroundColor={backgroundColor}
        color={color}
      >
        {title}
      </StyledButton>
    );
  }
}

Button.defaultProps = {
  action: Function.prototype,
  backgroundColor: 'black',
  color: 'white',
  size: 'regular',
  title: string,
};

Button.propTypes = {
  action: func,
  backgroundColor: string,
  color: string,
  size: oneOf(['small', 'regular', 'big']),
  title: string,
};

export default Button;
