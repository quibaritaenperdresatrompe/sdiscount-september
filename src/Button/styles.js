import styled from 'react-emotion';

const correspondingFontSize = {
  small: '0.8em',
  regular: '1em',
  big: '1.5em',
};

export const StyledButton = styled('button')(
  ({ backgroundColor, color, size }) => ({
    backgroundColor,
    border: 'none',
    color,
    cursor: 'pointer',
    fontSize: correspondingFontSize[size],
    outline: 'none',
    padding: '0.5em',
  }),
);
