import { number, string } from 'prop-types';
import React, { Component } from 'react';

import { StyledHeader, StyledLink } from './styles';

class Header extends Component {
  renderBasketLink() {
    const { productCount } = this.props;

    if (productCount === 0) return null;

    return (
      <StyledLink to="/basket">
        <span>{`Voir mon panier - ${productCount} produit(s)`}</span>
      </StyledLink>
    );
  }

  render() {
    const { title } = this.props;

    return (
      <StyledHeader>
        <StyledLink to="/">
          <h1>
            <span className="Header-title-emoji" role="img" aria-label="sushi">
              🍣
            </span>
            {title}
          </h1>
        </StyledLink>
        {this.renderBasketLink()}
      </StyledHeader>
    );
  }
}

Header.defaultProps = {
  productCount: 0,
  title: '',
};

Header.propTypes = {
  productCount: number,
  title: string,
};

export default Header;
