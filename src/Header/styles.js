import { Link } from 'react-router-dom';
import styled from 'react-emotion';

export const StyledHeader = styled('header')({
  alignItems: 'center',
  backgroundColor: 'black',
  color: 'whitesmoke',
  display: 'flex',
  height: '60px',
  justifyContent: 'space-between',
  paddingLeft: '1em',
  paddingRight: '1em',
});

export const StyledLink = styled(Link)({
  textDecoration: 'none',
  color: 'inherit',
});
