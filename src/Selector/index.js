import { arrayOf, bool, func, oneOf, shape, string } from 'prop-types';
import React, { Component } from 'react';

import './styles.css';

class Selector extends Component {
  renderOptions() {
    const { options } = this.props;

    return options.map(({ label, selected, value }) => (
      <option key={value} value={value} selected={selected}>
        {label}
      </option>
    ));
  }

  render() {
    const { value, onChange, options, size } = this.props;

    if (options.length === 0) return null;

    return (
      <select
        value={value}
        className={`selector selector-${size}`}
        onChange={onChange}
      >
        {this.renderOptions()}
      </select>
    );
  }
}

Selector.defaultProps = {
  value: null,
  onChange: Function.prototype,
  options: [],
  size: 'regular',
};

Selector.propTypes = {
  value: string,
  onChange: func,
  options: arrayOf(shape({ label: string, selected: bool, value: string })),
  size: oneOf(['small', 'regular', 'big']),
};

export default Selector;
