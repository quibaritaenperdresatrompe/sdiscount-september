import { connect } from 'react-redux';

import Header from '../Header';

const mapStateToProps = state => ({
  productCount: state.products.reduce((sum, { quantity }) => sum + quantity, 0),
});

export default connect(
  mapStateToProps,
  null,
)(Header);
