import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { setProducts } from '../redux/actions';
import App from '../App';

const mapStateToProps = state => ({
  products: state.products,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setProducts,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
