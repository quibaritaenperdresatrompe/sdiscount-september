import { arrayOf, func, shape } from 'prop-types';
import { Redirect } from 'react-router-dom';
import React, { Component, Fragment } from 'react';

import { basketItemTypes } from '../BasketItem';
import { StyledBasket, StyledBasketItems } from './styles';
import BasketItem from '../ConnectedBasketItem';
import Button from '../Button';

class Basket extends Component {
  constructor() {
    super();

    this.state = {
      showConfirmMessage: false,
    };
  }

  handleRemoveAllButtonClick = () => {
    const { emptyBasket } = this.props;

    emptyBasket();
  };

  handleConfirmButtonClick = () => {
    const { emptyBasket } = this.props;

    this.showConfirmMessage();
    emptyBasket();
    setTimeout(this.hideConfirmMessage, 3000);
  };

  showConfirmMessage = () => {
    this.setState(() => ({
      showConfirmMessage: true,
    }));
  };

  hideConfirmMessage = () => {
    this.setState(() => ({
      showConfirmMessage: false,
    }));
  };

  renderItems() {
    const { items } = this.props;

    return items.map(({ reference, ...otherProps }) => (
      <BasketItem key={reference} reference={reference} {...otherProps} />
    ));
  }

  renderConfirmMessage() {
    const { showConfirmMessage } = this.state;

    if (!showConfirmMessage) return null;

    return <h3>Votre commande a bien été prise en compte</h3>;
  }

  renderContent() {
    const { items } = this.props;

    if (items.length === 0) return null;

    const totalCount = items.reduce((sum, { quantity }) => sum + quantity, 0);
    const totalPrice = items.reduce(
      (sum, { price, quantity }) => sum + quantity * price,
      0,
    );

    return (
      <Fragment>
        <h2>{`${totalPrice.toFixed(2)} € - ${totalCount} produits(s)`}</h2>
        <Button
          action={this.handleRemoveAllButtonClick}
          size="small"
          title="Vider mon panier"
        />
        <StyledBasketItems>{this.renderItems()}</StyledBasketItems>
        <Button
          action={this.handleConfirmButtonClick}
          size="big"
          title="Valider ma commande"
        />
      </Fragment>
    );
  }

  render() {
    const { items } = this.props;
    const { showConfirmMessage } = this.state;

    if (items.length === 0 && !showConfirmMessage) return <Redirect to="/" />;

    return (
      <StyledBasket>
        {this.renderContent()}
        {this.renderConfirmMessage()}
      </StyledBasket>
    );
  }
}

Basket.defaultProps = {
  emptyBasket: Function.prototype,
  items: [],
};

Basket.propTypes = {
  emptyBasket: func,
  items: arrayOf(shape(basketItemTypes)),
};

export default Basket;
