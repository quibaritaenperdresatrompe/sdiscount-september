import styled from 'react-emotion';

export const StyledBasket = styled('div')({
  color: 'black',
  padding: '1em',
});

export const StyledBasketItems = styled('ol')({
  display: 'flex',
  flexDirection: 'column',
  margin: 0,
});
