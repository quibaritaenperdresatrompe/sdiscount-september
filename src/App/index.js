import { func } from 'prop-types';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import React, { Component, Fragment } from 'react';

import Basket from '../ConnectedBasket';
import Header from '../ConnectedHeader';
import ProductList from '../ConnectedProductList';

class App extends Component {
  componentDidMount() {
    const { setProducts } = this.props;

    fetch('https://sdiscount-api.herokuapp.com/products')
      .then(response => response.json())
      .then(data => {
        const products = data.map(product => ({ ...product, quantity: 0 }));

        setProducts(products);
      });
  }

  render() {
    return (
      <Router>
        <Fragment>
          <Header title="Sdiscount" />
          <Route exact path="/" component={ProductList} />
          <Route exact path="/basket" component={Basket} />
        </Fragment>
      </Router>
    );
  }
}

App.propTypes = {
  setProducts: func,
};

App.defaultProps = {
  setProducts: Function.prototype,
};

export default App;
