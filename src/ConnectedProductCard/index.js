import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { updateProductQuantity } from '../redux/actions';
import ProductCard from '../ProductCard';

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateProductQuantity,
    },
    dispatch,
  );

export default connect(
  null,
  mapDispatchToProps,
)(ProductCard);
