import { func, number, string } from 'prop-types';
import React, { Component } from 'react';

import Button from '../Button';
import Selector from '../Selector';
import './styles.css';

class ProductCard extends Component {
  constructor() {
    super();

    this.state = {
      selectedQuantity: 1,
    };
  }

  handleAddButtonClick = () => {
    const { reference, updateProductQuantity } = this.props;
    const { selectedQuantity } = this.state;

    updateProductQuantity(reference, selectedQuantity);

    this.setState(() => ({
      selectedQuantity: 1,
    }));
  };

  handleQuantitySelectorChange = event => {
    if (event && event.target && event.target.value) {
      const selectedQuantity = parseInt(event.target.value, 10);

      this.setState(() => ({
        selectedQuantity,
      }));
    }
  };

  renderQuantitySelector() {
    const { price } = this.props;
    const { selectedQuantity } = this.state;
    const availableQuantities = [1, 2, 3, 4, 5, 10];
    const options = availableQuantities.map(quantity => ({
      label: `${quantity} - ${(quantity * price).toFixed(2)} €`,
      value: quantity.toString(),
    }));

    return (
      <Selector
        value={selectedQuantity.toString()}
        onChange={this.handleQuantitySelectorChange}
        options={options}
      />
    );
  }

  render() {
    const { description, imageUrl, price, title } = this.props;

    if (!price) return null;

    return (
      <div className="product-card">
        <div className="product-card-image-container">
          <img className="product-card-image" src={imageUrl} alt={title} />
        </div>
        <div className="product-card-actions">
          {this.renderQuantitySelector()}
          <Button action={this.handleAddButtonClick} title="Ajouter" />
        </div>
        <div className="product-card-content">
          <h2>{title}</h2>
          <p>{description}</p>
        </div>
      </div>
    );
  }
}

ProductCard.defaultProps = {
  imageUrl: '',
  title: '',
  description: '',
  price: 0,
  reference: '',
  updateProductQuantity: Function.prototype,
};

export const productCardTypes = {
  imageUrl: string,
  title: string,
  description: string,
  price: number,
  reference: string,
};

ProductCard.propTypes = {
  ...productCardTypes,
  updateProductQuantity: func,
};

export default ProductCard;
