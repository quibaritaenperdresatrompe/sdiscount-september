import itRendersAllMutations from '../../../lib/it-renders-all-mutations';

import ProductCard from '../';

const mutations = [
  {
    name: 'with all props',
    props: {
      price: 9.99,
      title: 'Amazing title',
      description: 'Wonderful description',
      imageUrl: 'http://',
      reference: '000',
      updateProductQuantity: jest.fn(),
    },
  },
  {
    name: 'without price',
    props: {
      title: 'Amazing title',
      description: 'Wonderful description',
      imageUrl: 'http://',
      reference: '000',
      updateProductQuantity: jest.fn(),
    },
  },
];

describe(ProductCard.name, () => {
  itRendersAllMutations(ProductCard, mutations);

  describe('handlers', () => {
    describe('handleAddButtonClick', function scope() {
      beforeEach(() => {
        this.ProductCard = new ProductCard({});
      });

      test('it calls updateProductQuantity and setState', () => {
        this.ProductCard.props = {
          reference: '001',
          updateProductQuantity: jest.fn(),
        };

        this.ProductCard.state = { selectedQuantity: 9 };

        this.ProductCard.setState = jest.fn();

        this.ProductCard.handleAddButtonClick();

        expect(
          this.ProductCard.props.updateProductQuantity,
        ).toHaveBeenCalledWith(
          this.ProductCard.props.reference,
          this.ProductCard.state.selectedQuantity,
        );

        expect(this.ProductCard.setState).toHaveBeenCalled();
      });
    });

    describe('handleQuantitySelectorChange', function scope() {
      beforeEach(() => {
        this.ProductCard = new ProductCard({});
      });

      test('it calls setState with event', () => {
        const event = {
          target: {
            value: '1',
          },
        };

        this.ProductCard.setState = jest.fn();

        this.ProductCard.handleQuantitySelectorChange(event);

        expect(this.ProductCard.setState).toHaveBeenCalled();
      });

      test('it does nothing without event.target.value', () => {
        const event = {
          target: {},
        };

        this.ProductCard.setState = jest.fn();

        this.ProductCard.handleQuantitySelectorChange(event);

        expect(this.ProductCard.setState).not.toHaveBeenCalled();
      });
    });
  });
});
