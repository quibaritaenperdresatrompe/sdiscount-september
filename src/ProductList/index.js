import { arrayOf, shape } from 'prop-types';
import React, { Component } from 'react';

import { productCardTypes } from '../ProductCard';
import ProductCard from '../ConnectedProductCard';
import './styles.css';

class ProductList extends Component {
  renderProducts() {
    const { products } = this.props;

    return products.map(({ reference, ...otherProps }) => (
      <ProductCard key={reference} reference={reference} {...otherProps} />
    ));
  }

  render() {
    return <div className="product-list">{this.renderProducts()}</div>;
  }
}

ProductList.defaultProps = {
  products: [],
};

ProductList.propTypes = {
  products: arrayOf(shape(productCardTypes)),
};

export default ProductList;
