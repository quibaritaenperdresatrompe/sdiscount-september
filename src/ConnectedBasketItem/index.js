import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { removeProduct, updateProductQuantity } from '../redux/actions';
import BasketItem from '../BasketItem';

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      removeProduct,
      updateProductQuantity,
    },
    dispatch,
  );

export default connect(
  null,
  mapDispatchToProps,
)(BasketItem);
